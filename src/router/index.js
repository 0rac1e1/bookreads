import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Home from '@/components/Home'
import Register from '@/components/Register'
import NewBook from '@/components/NewBook'
import BookDetailsPage from '@/components/BookDetailsPage'

Vue.use(Router)

let router = new Router({
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Home,
		},
		{
			path: '/login',
			name: 'Login',
			component: Login
		},
		{
			path: '/register',
			name: 'Register',
			component: Register
		},
		{
			path: '/add-book',
			name: 'NewBook',
			component: NewBook,
			meta: {
				requiresAuth: false
			}
		},
		{
			path: '/book-detail',
			name: 'BookDetailsPage',
			component: BookDetailsPage,
			props: true,
			meta: {
				requiresAuth: false
			}
		}
	]
})

router.beforeEach((to, from, next) => {

	let currentUser = JSON.parse(localStorage.getItem('currentUser'));

	let requiresAuth = to.matched.some(record => record.meta.requiresAuth);

	if (requiresAuth && !currentUser) {
		next('/')
	} else if (!requiresAuth && currentUser) {
		next('home')
	} else {
		next()
	}

})

export default router